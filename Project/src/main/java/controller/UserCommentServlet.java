package controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.CommentDao;
import dao.ThreadDao;
import model.CommentDataBeans;
import model.ThreadDataBeans;
import model.UserDataBeans;

/**
 * Servlet implementation class UserCommentServlet
 */
@WebServlet("/UserCommentServlet")
public class UserCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserCommentServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");
      ThreadDao threadDao = new ThreadDao();
      CommentDao commentDao = new CommentDao();

      // TODOログインセッションがない場合、ログイン画面にリダイレクトさせる
      HttpSession session = request.getSession();
      UserDataBeans loginUser = (UserDataBeans) session.getAttribute("userInfo");
      if (loginUser == null) {
        response.sendRedirect("LoginServlet");
        return;
      }
      int thread_id = Integer.valueOf(request.getParameter("threadId"));
      ThreadDataBeans thread = threadDao.findThreadData(thread_id);
      request.setAttribute("thread", thread);

      // コメント一覧を取得
      int user_id = Integer.valueOf(request.getParameter("userId"));
      List<CommentDataBeans> commentList = commentDao.findUserComment(thread_id, user_id);
      request.setAttribute("commentList", commentList);
      // ユーザーコメント一覧のjspにフォワード
      request.getRequestDispatcher("/WEB-INF/jsp/user_comment.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
