package controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.CommentDao;
import dao.FavoriteDao;
import dao.ThreadDao;
import dao.UserDao;
import model.CommentDataBeans;
import model.ThreadDataBeans;
import model.UserDataBeans;

/**
 * Servlet implementation class ThreadCommentServlet
 */
@WebServlet("/ThreadCommentServlet")
public class ThreadCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ThreadCommentServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");
      UserDao userDao = new UserDao();
      ThreadDao threadDao = new ThreadDao();
      CommentDao commentDao = new CommentDao();

      // TODOログインセッションがない場合、ログイン画面にリダイレクトさせる
      HttpSession session = request.getSession();
      UserDataBeans loginUser = (UserDataBeans) session.getAttribute("userInfo");
      if (loginUser == null) {
        response.sendRedirect("LoginServlet");
        return;
      }
      // スレッド情報を取得
      int id = Integer.valueOf(request.getParameter("id"));
      ThreadDataBeans thread = threadDao.findThreadData(id);
      request.setAttribute("thread", thread);

      // コメント一覧を取得
      List<CommentDataBeans> commentList = commentDao.findCommentData(id);
      request.setAttribute("commentList", commentList);

      // コメント一覧のjspにフォワード
      request.getRequestDispatcher("/WEB-INF/jsp/thread_comment.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");
      HttpSession session = request.getSession();
      ThreadDao threadDao = new ThreadDao();
      CommentDao commentDao = new CommentDao();
      FavoriteDao favoriteDao = new FavoriteDao();

      // お気に入りを変更する
      UserDataBeans userInfo = (UserDataBeans) session.getAttribute("userInfo");
      int userId = Integer.valueOf(userInfo.getId());
      int threadId = Integer.valueOf(request.getParameter("thread-id"));
      favoriteDao.change(userId, threadId);

      // スレッド情報を取得
      ThreadDataBeans thread = threadDao.findThreadData(threadId);
      request.setAttribute("thread", thread);

      // コメント一覧を取得
      List<CommentDataBeans> commentList = commentDao.findCommentData(threadId);
      request.setAttribute("commentList", commentList);
      // コメント一覧のjspにフォワード
      request.getRequestDispatcher("/WEB-INF/jsp/thread_comment.jsp").forward(request, response);

	}

}
