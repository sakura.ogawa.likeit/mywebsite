package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.UserDataBeans;

/**
 * Servlet implementation class UserDeleteServlet
 */
@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public UserDeleteServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // TODOログインセッションがない場合、ログイン画面にリダイレクトさせる
    HttpSession session = request.getSession();

    UserDataBeans loginUser = (UserDataBeans) session.getAttribute("userInfo");
    if (loginUser == null) {
      response.sendRedirect("LoginServlet");
      return;
    }
    // ユーザー情報削除のjspにフォワード
    request.getRequestDispatcher("/WEB-INF/jsp/user_delete.jsp").forward(request, response);
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession();
    UserDao userDao = new UserDao();


    // idを取得し、型変換する
    UserDataBeans userInfo = (UserDataBeans) session.getAttribute("userInfo");
    int id = Integer.valueOf(userInfo.getId());
    userDao.delete(id); // UserDaoのインスタンスを作り、deleteメソッドに引数を飛ばす

    // ユーザー情報削除確認のjspにフォワード
    request.getRequestDispatcher("/WEB-INF/jsp/delete_result.jsp").forward(request, response);
  }

}
