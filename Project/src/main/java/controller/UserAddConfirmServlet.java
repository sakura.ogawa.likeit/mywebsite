package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import dao.UserDao;
import util.PasswordEncorder;

/**
 * Servlet implementation class UserAddConfirm
 */
@WebServlet("/UserAddConfirmServlet")
public class UserAddConfirmServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserAddConfirmServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");
      UserDao userDao = new UserDao();
      // リクエストパラメータの入力項目を取得
      String loginId = request.getParameter("loginId"); // user_add_confirm.jspから飛んできた
      String password = request.getParameter("password");
      String name = request.getParameter("user-name");

      // 登録が確定されたかどうか確認するための変数
      String confirmed = request.getParameter("confirm_button");

      switch (confirmed) {
        case "cancel":
          request.setAttribute("loginId", loginId);
          request.setAttribute("userName", name);
          // 新規登録のjspにフォワード
          request.getRequestDispatcher("/WEB-INF/jsp/user_add_correct.jsp").forward(request,
              response);
          break;

        case "regist":
          PasswordEncorder passwordEncorder = new PasswordEncorder();
          String Epassword = passwordEncorder.encordPassword(password);
          userDao.insert(loginId, name, Epassword); // UserDaoのインスタンスを作り、insertメソッドに引数を飛ばす
          request.getRequestDispatcher("/WEB-INF/jsp/confirm_result.jsp").forward(request,
              response);
          break;
      }
	}

}
