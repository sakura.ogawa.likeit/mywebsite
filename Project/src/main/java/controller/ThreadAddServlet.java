package controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.ThreadDao;
import model.ThreadDataBeans;
import model.UserDataBeans;

/**
 * Servlet implementation class ThreadAddServlet
 */
@WebServlet("/ThreadAddServlet")
public class ThreadAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ThreadAddServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // TODOログインセッションがない場合、ログイン画面にリダイレクトさせる
      HttpSession session = request.getSession();
      UserDataBeans loginUser = (UserDataBeans) session.getAttribute("userInfo");
      if (loginUser == null) {
        response.sendRedirect("LoginServlet");
        return;
      }
      // フォワード
      request.getRequestDispatcher("/WEB-INF/jsp/thread_add.jsp").forward(request, response);
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");
      ThreadDao threadDao = new ThreadDao();
      // リクエストパラメータの入力項目を取得
      String userId = request.getParameter("user-id");
      int id = Integer.valueOf(userId);
      String userName = request.getParameter("user-name"); // thread_add.jspから飛んできた
      String title = request.getParameter("title");
      String overview = request.getParameter("overview");

      String errMsg = "";

      /** 入力していない項目がある場合 * */
      if (!isInputAll(title, overview)) {
        // メソッド名を決める際、戻り値の型がbooleanであれば「is,has,can,exist」から始めるというルールがある

        errMsg += "入力されていない項目があります";
      }

      if (errMsg.length() == 0) {
        /** 情報が全て入力されている * */
        threadDao.insert(id, userName, title, overview);

        // スレッド一覧を取得
        List<ThreadDataBeans> threadList = threadDao.findAll();
        request.setAttribute("threadList", threadList);

        // スレッド一覧のjspにフォワード
        request.getRequestDispatcher("/WEB-INF/jsp/thread_list.jsp").forward(request, response);

      } else {
        // 入力した名前と生年月日を画面に表示するため、リクエストに値をセット
        request.setAttribute("title", title);
        request.setAttribute("overview", overview);
        request.setAttribute("errMsg", errMsg);
        // user_add.jspにフォワード
        request.getRequestDispatcher("/WEB-INF/jsp/thread_add.jsp").forward(request, response);
        return;
      }
    }

    private static boolean isInputAll(String title, String overview) {
      return !(title.equals("") || overview.equals(""));
    }

}
