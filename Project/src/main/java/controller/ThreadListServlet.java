package controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.ThreadDao;
import dao.UserDao;
import model.ThreadDataBeans;
import model.UserDataBeans;

/**
 * Servlet implementation class ThreadLiseServlet
 */
@WebServlet("/ThreadListServlet")
public class ThreadListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ThreadListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      UserDao userDao = new UserDao();
      ThreadDao threadDao = new ThreadDao();

      // TODOログインセッションがない場合、ログイン画面にリダイレクトさせる
      HttpSession session = request.getSession();
      UserDataBeans loginUser = (UserDataBeans) session.getAttribute("userInfo");
      if (loginUser == null) {
        response.sendRedirect("LoginServlet");
        return;
      }
      // スレッド一覧を取得
      List<ThreadDataBeans> threadList = threadDao.findAll();
      request.setAttribute("threadList", threadList);

      // フォワード
      request.getRequestDispatcher("/WEB-INF/jsp/thread_list.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	  ThreadDao threadDao = new ThreadDao();

      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      // 表示の切り替えを判断するための変数
      String confirmed = request.getParameter("change_button");

      switch (confirmed) {
        case "new":
          // スレッド一覧を取得
          List<ThreadDataBeans> threadList = threadDao.findAll();
          request.setAttribute("threadList", threadList);

          // フォワード
          request.getRequestDispatcher("/WEB-INF/jsp/thread_list.jsp").forward(request, response);
          break;

        case "popular":
          // スレッド一覧を取得
          List<ThreadDataBeans> PopularList = threadDao.findPopular();
          request.setAttribute("threadList", PopularList);

          // フォワード
          request.getRequestDispatcher("/WEB-INF/jsp/thread_list.jsp").forward(request, response);
          break;
	}
  }

}

