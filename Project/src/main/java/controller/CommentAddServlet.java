package controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.CommentDao;
import dao.ThreadDao;
import model.CommentDataBeans;
import model.ThreadDataBeans;
import model.UserDataBeans;

/**
 * Servlet implementation class CommentaddServlet
 */
@WebServlet("/CommentAddServlet")
public class CommentAddServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public CommentAddServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // TODO Auto-generated method stub
    response.getWriter().append("Served at: ").append(request.getContextPath());
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
 // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("UTF-8");
    ThreadDao threadDao = new ThreadDao();
    CommentDao commentDao = new CommentDao();
    
    // セッションを取得
    HttpSession session = request.getSession();
    UserDataBeans userInfo = (UserDataBeans) session.getAttribute("userInfo");
    int userId = userInfo.getId();
    String userName = userInfo.getName();


    // リクエストパラメータの入力項目を取得
    int threadId = Integer.valueOf(request.getParameter("thread-id"));
    String comment = request.getParameter("comment");


    /** パスワードが一致しない、入力していない項目がある場合 * */
    if (comment.equals("")) {
      String errMsg = "コメントが入力されていません";
      request.setAttribute("errMsg", errMsg);

      // thread_comment.jspにフォワード
      request.getRequestDispatcher("/WEB-INF/jsp/thread_comment.jsp").forward(request, response);
      return;

    } else {
      /** パスワードが一致、情報が全て入力されている * */
      commentDao.insert(threadId, userId, userName, comment);

      ThreadDataBeans thread = threadDao.findThreadData(threadId);
      request.setAttribute("thread", thread);

      // コメント一覧を取得
      List<CommentDataBeans> commentList = commentDao.findCommentData(threadId);
      request.setAttribute("commentList", commentList);
      // コメント一覧のjspにフォワード
      request.getRequestDispatcher("/WEB-INF/jsp/thread_comment.jsp").forward(request, response);
      return;
    }
  }
}
