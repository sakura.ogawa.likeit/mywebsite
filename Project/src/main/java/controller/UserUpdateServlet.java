package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.UserDataBeans;
import util.PasswordEncorder;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // TODOログインセッションがない場合、ログイン画面にリダイレクトさせる
      HttpSession session = request.getSession();
      UserDataBeans loginUser = (UserDataBeans) session.getAttribute("userInfo");
      if (loginUser == null) {
        response.sendRedirect("LoginServlet");
        return;
      }
      // // フォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_update.jsp"); // userUpdate.jspへuserDateをとばす
      dispatcher.forward(request, response);
      return;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      HttpSession session = request.getSession();

      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");
      UserDao userDao = new UserDao();
      // リクエストパラメータの入力項目を取得
      String loginId = request.getParameter("loginId");
      String password = request.getParameter("password");
      String cPassword = request.getParameter("password-confirm");
      String name = request.getParameter("user-name");

      String errMsg = "";

      /** パスワードが一致しない、入力していない項目がある場合 * */
      if (!isInputAll(loginId, password, cPassword, name) || !(password.equals(cPassword))) {
        // メソッド名を決める際、戻り値の型がbooleanであれば「is,has,can,exist」から始めるというルールがある

        errMsg += "入力された内容は正しくありません";
      }

      if (errMsg.length() == 0) {
        /** パスワードが一致、情報が全て入力されている * */
        request.setAttribute("loginId", loginId);
        request.setAttribute("password", password);
        request.setAttribute("user-name", name);

        PasswordEncorder passwordEncorder = new PasswordEncorder();
        String Epassword = passwordEncorder.encordPassword(password);
        userDao.update(loginId, name, Epassword); // UserDaoのインスタンスを作り、updateメソッドに引数を飛ばす

        // ログイン時に保存したセッション内のユーザ情報を削除
        session.removeAttribute("userInfo");

        // 更新したユーザー情報を取得
        UserDataBeans user = userDao.getUserData(loginId, Epassword);
        session.setAttribute("userInfo", user);

        // ユーザー情報更新完了のサーブレットにフォワード
        request.getRequestDispatcher("/WEB-INF/jsp/update_result.jsp").forward(request, response);
      } else {
        // 入力した名前と生年月日を画面に表示するため、リクエストに値をセット
        request.setAttribute("name", name);
        request.setAttribute("errMsg", errMsg);
        // user_add.jspにフォワード
        request.getRequestDispatcher("/WEB-INF/jsp/user_update.jsp").forward(request, response);
        return;
      }
    }

    private static boolean isInputAll(String loginId, String password, String confirmPassword,
        String name) {
      return !(loginId.equals("") || password.equals("") || confirmPassword.equals("")
          || name.equals(""));
    }


}
