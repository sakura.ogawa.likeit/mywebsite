package controller;

public class MWSHelper {
  // 検索結果
  static final String SEARCH_RESULT_PAGE = "/WEB-INF/jsp/itemsearchresult.jsp";
  // 商品ページ
  static final String ITEM_PAGE = "/WEB-INF/jsp/item.jsp";
  // スレッド一覧
  static final String THREAD_LIST = "/WEB-INF/jsp/thread_list.jsp";
  // 新規スレッド登録
  static final String THREAD_ADD = "/WEB-INF/jsp/thread_add.jsp";
  // スレッド削除
  static final String THREAD_COMMENT_DELETE = "/WEB-INF/jsp/thread_comment_delete.jsp";
  // スレッド削除確認
  static final String THREAD_DELETE_RESULT = "/WEB-INF/jsp/thread_delete_result.jsp";
  // コメント一覧
  static final String THREAD_COMMENT = "/WEB-INF/jsp/thread_comment.jsp";
  // コメント削除
  static final String COMMENT_DELETE = "/WEB-INF/jsp/comment_delete.jsp";
  // お気に入り一覧
  static final String FAVORITE_THREAD = "/WEB-INF/jsp/favorite_thread.jsp";
  // お気に入り全削除
  static final String FAVORITE_DELETE = "/WEB-INF/jsp/favorite_delete.jsp";
  // お気に入り削除確認
  static final String FAVORITE_RESULT = "/WEB-INF/jsp/favorite_result.jsp";
  // 返信コメント一覧
  static final String REPLY_COMMENT = "/WEB-INF/jsp/reply_comment.jsp";
  // 返信コメント登録
  static final String REPLY_ADD = "/WEB-INF/jsp/reply_add.jsp";
  // ユーザーコメント一覧
  static final String USER_COMMENT = "/WEB-INF/jsp/user_comment.jsp";
  // ユーザー情報詳細
  static final String USER_DATAIL = "/WEB-INF/jsp/user_datail.jsp";
  // ユーザー情報更新
  static final String USER_UPDATE = "/WEB-INF/jsp/user_update.jsp";
  // ユーザー情報更新完了
  static final String UPDATE_RESULT = "/WEB-INF/jsp/update_result.jsp";
  // ユーザー情報削除
  static final String USER_DELETE = "/WEB-INF/jsp/user_delete.jsp";
  // ユーザー情報削除確認
  static final String DELETE_RESULT = "/WEB-INF/jsp/delete_result.jsp";
  // ログイン
  static final String LOGIN = "/WEB-INF/jsp/login.jsp";
  // ログアウト
  static final String LOGOUT = "/WEB-INF/jsp/logout.jsp";
  // 新規登録
  static final String USER_ADD = "/WEB-INF/jsp/user_add.jsp";
  // 新規登録入力内容確認
  static final String USER_ADD_CONFIRM = "/WEB-INF/jsp/user_add_confirm.jsp";
  // 新規登録完了
  static final String CONFIRM_RESULT = "/WEB-INF/jsp/confirm_result.jsp";

  public static MWSHelper getInstance() {
    return new MWSHelper();
  }

}
