package controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import dao.ThreadDao;
import model.ThreadDataBeans;

/**
 * Servlet implementation class SearchResultServlet
 */
@WebServlet("/SearchResultServlet")
public class SearchResultServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public SearchResultServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    ThreadDao threadDao = new ThreadDao();

    // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("UTF-8");
    String title = request.getParameter("title");

    // threadDaoのsearchメソッドで詳細情報を手に入れる
    if (title.equals("")) {
      List<ThreadDataBeans> threadList = threadDao.findAll();
      request.setAttribute("threadList", threadList);
      // フォワード
      request.getRequestDispatcher("/WEB-INF/jsp/search_result.jsp").forward(request, response);
    }

    // リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
    List<ThreadDataBeans> threadList = threadDao.search(title);

    // テーブルに該当のデータが見つからなかった場合
    if (threadList.isEmpty()) {
      // リクエストスコープにエラーメッセージをセット
      request.setAttribute("errMsg", "スレッドが見つかりませんでした");
    }
    request.setAttribute("threadList", threadList);
    // フォワード
    request.getRequestDispatcher("/WEB-INF/jsp/search_result.jsp").forward(request, response);
  }

}
