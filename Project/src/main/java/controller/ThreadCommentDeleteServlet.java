package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.ThreadDao;
import model.ThreadDataBeans;
import model.UserDataBeans;

/**
 * Servlet implementation class ThreadCommentDeleteServlet
 */
@WebServlet("/ThreadCommentDeleteServlet")
public class ThreadCommentDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ThreadCommentDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");
      ThreadDao threadDao = new ThreadDao();
      // ログインセッションがない場合、ログイン画面にリダイレクトさせる
      HttpSession session = request.getSession();
      UserDataBeans loginUser = (UserDataBeans) session.getAttribute("userInfo");
      if (loginUser == null) {
        response.sendRedirect("LoginServlet");
        return;
      }
      int id = Integer.valueOf(request.getParameter("id"));
      ThreadDataBeans thread = threadDao.findThreadData(id);
      request.setAttribute("thread", thread);

      // スレッド削除確認のjspにフォワード
      request.getRequestDispatcher("/WEB-INF/jsp/thread_comment_delete.jsp").forward(request,
          response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");
      ThreadDao threadDao = new ThreadDao();
      int id = Integer.valueOf(request.getParameter("thread-id"));
      threadDao.delete(id);
      // スレッド削除完了のjspにフォワード
      request.getRequestDispatcher("/WEB-INF/jsp/thread_delete_result.jsp").forward(request,
          response);
	}

}
