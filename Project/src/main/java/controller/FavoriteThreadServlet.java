package controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.FavoriteDao;
import model.ThreadDataBeans;
import model.UserDataBeans;

/**
 * Servlet implementation class FavoriteThreadServlet
 */
@WebServlet("/FavoriteThreadServlet")
public class FavoriteThreadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FavoriteThreadServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      FavoriteDao favoriteDao = new FavoriteDao();

      // ログインセッションがない場合、ログイン画面にリダイレクトさせる
      HttpSession session = request.getSession();
      UserDataBeans loginUser = (UserDataBeans) session.getAttribute("userInfo");
      if (loginUser == null) {
        response.sendRedirect("LoginServlet");
        return;
      }

      // idを取得し、型変換する
      UserDataBeans userInfo = (UserDataBeans) session.getAttribute("userInfo");
      int id = Integer.valueOf(userInfo.getId());

      // スレッド一覧を取得
      List<ThreadDataBeans> threadList = favoriteDao.getFavoriteData(id);

      // テーブルに該当のデータが見つからなかった場合
      if (threadList.isEmpty()) {
        // リクエストスコープにエラーメッセージをセット
        request.setAttribute("errMsg", "お気に入りが見つかりませんでした");
      }
      request.setAttribute("threadList", threadList);

      // お気に入り一覧のjspにフォワード
      request.getRequestDispatcher("/WEB-INF/jsp/favorite_thread.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");
      HttpSession session = request.getSession();
      FavoriteDao favoriteDao = new FavoriteDao();

      // お気に入りを変更
      UserDataBeans userInfo = (UserDataBeans) session.getAttribute("userInfo");
      int userId = Integer.valueOf(userInfo.getId());
      int threadId = Integer.valueOf(request.getParameter("thread-id"));
      favoriteDao.change(userId, threadId);

      // スレッド一覧を取得
      List<ThreadDataBeans> threadList = favoriteDao.getFavoriteData(userId);

      // テーブルに該当のデータが見つからなかった場合
      if (threadList.isEmpty()) {
        // リクエストスコープにエラーメッセージをセット
        request.setAttribute("errMsg", "お気に入りが見つかりませんでした");
      }

      request.setAttribute("threadList", threadList);

      // お気に入り一覧のjspにフォワード
      request.getRequestDispatcher("/WEB-INF/jsp/favorite_thread.jsp").forward(request, response);
	}

}
