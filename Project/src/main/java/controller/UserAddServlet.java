package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import dao.UserDao;

/**
 * Servlet implementation class UserAddServlet
 */
@WebServlet("/UserAddServlet")
public class UserAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserAddServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
      // フォワード
      request.getRequestDispatcher("/WEB-INF/jsp/user_add.jsp").forward(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");
      UserDao userDao = new UserDao();
      // リクエストパラメータの入力項目を取得
      String loginId = request.getParameter("loginId"); // userAdd.jspから飛んできた
      String password = request.getParameter("password");
      String cPassword = request.getParameter("password-confirm");
      String name = request.getParameter("user-name");

      String errMsg = "";

      /** パスワードが一致しない、入力していない項目がある場合 * */
      if (!isInputAll(loginId, password, cPassword, name) || !(password.equals(cPassword))) {
        // メソッド名を決める際、戻り値の型がbooleanであれば「is,has,can,exist」から始めるというルールがある

        errMsg += "入力された内容は正しくありません";
      }
      if (userDao.existUser(loginId)) {

        errMsg += "ほかのユーザーが使用中のログインIDです";
      }
      if (errMsg.length() == 0) {
        /** パスワードが一致、情報が全て入力されている * */
        request.setAttribute("loginId", loginId);
        request.setAttribute("password", password);
        request.setAttribute("userName", name);

        // 新規登録確認のjspにフォワード
        request.getRequestDispatcher("/WEB-INF/jsp/user_add_confirm.jsp").forward(request,
            response);
      } else {
        // 入力した名前と生年月日を画面に表示するため、リクエストに値をセット
        request.setAttribute("loginId", loginId);
        request.setAttribute("userName", name);
        request.setAttribute("errMsg", errMsg);
        // user_add.jspにフォワード
        request.getRequestDispatcher("/WEB-INF/jsp/user_add.jsp").forward(request, response);
        return;
      }
    }

    private static boolean isInputAll(String loginId, String password, String confirmPassword,
        String name) {
      return !(loginId.equals("") || password.equals("") || confirmPassword.equals("")
          || name.equals(""));
    }


  }
