package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.CommentDataBeans;

public class CommentDao {
  public List<CommentDataBeans> findCommentData(int threadId) {
    Connection con = null;
    PreparedStatement pStmt = null;
    List<CommentDataBeans> commentList = new ArrayList<CommentDataBeans>();

    try {
      // データベースへ接続
      con = DBManager.getConnection();

      pStmt = con.prepareStatement("SELECT comment.*,"
          + " (SELECT COUNT(*) FROM comment c WHERE c.thread_id = comment.thread_id AND c.create_date <= comment.create_date) AS new_comment_id"
          + " FROM comment"
          + " WHERE thread_id = ?"
          + " ORDER BY create_date ASC;");

      // SELECTを実行し、結果表を取得
      pStmt.setInt(1, threadId);
      ResultSet rs = pStmt.executeQuery();

      // 結果表に格納されたレコードの内容を
      // Commentインスタンスに設定し、Listに追加

      while (rs.next()) {
        CommentDataBeans comment = new CommentDataBeans();
        comment.setId(rs.getInt("id"));
        comment.setNewId(rs.getInt("new_comment_id"));
        comment.setThreadId(rs.getInt("thread_id"));
        comment.setUserId(rs.getInt("user_id"));
        comment.setUserName(rs.getString("user_name"));
        comment.setComment(rs.getString("comment"));
        comment.setCreateDate(rs.getTimestamp("create_date"));
        commentList.add(comment);
      }
      System.out.println("searching commentList has been completed");
      return commentList;
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (con != null) {
        try {
          con.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  public List<CommentDataBeans> findUserComment(int threadId, int userId) {
    Connection con = null;
    PreparedStatement pStmt = null;
    List<CommentDataBeans> commentList = new ArrayList<CommentDataBeans>();

    try {
      // データベースへ接続
      con = DBManager.getConnection();

      pStmt = con.prepareStatement("SELECT * FROM comment WHERE thread_id = ? and user_id = ?");

      // SELECTを実行し、結果表を取得
      pStmt.setInt(1, threadId);
      pStmt.setInt(2, userId);
      ResultSet rs = pStmt.executeQuery();

      // 結果表に格納されたレコードの内容を
      // Threadインスタンスに設定し、Listに追加

      while (rs.next()) {
        CommentDataBeans comment = new CommentDataBeans();
        comment.setId(rs.getInt("id"));
        comment.setThreadId(rs.getInt("thread_id"));
        comment.setUserId(rs.getInt("user_id"));
        comment.setUserName(rs.getString("user_name"));
        comment.setComment(rs.getString("comment"));
        comment.setCreateDate(rs.getTimestamp("create_date"));
        commentList.add(comment);
      }
      System.out.println("searching commentList has been completed");
      return commentList;
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (con != null) {
        try {
          con.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }
  public void insert(int threadId, int userId, String userName, String comment) {
    Connection con = null;
    PreparedStatement pStmt = null;
    try {
      // データベースへ接続
      con = DBManager.getConnection(); // DBManagerでコネクション

      // INSERT文を準備
      pStmt = con.prepareStatement(
          "INSERT INTO comment(thread_id,user_id,user_name,comment,create_date)VALUES(?,?,?,?,now())"); // 検索文用意飛んできたもので調べるため?を使う

      // INSERTを実行し、実行出来た数を表示
      pStmt.setInt(1, threadId);
      pStmt.setInt(2, userId);
      pStmt.setString(3, userName);
      pStmt.setString(4, comment);
      pStmt.executeUpdate();
      System.out.println("inserting comment has been completed");
      pStmt.close();
    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (con != null) {
        try {
          con.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public void delete(int commentId) {
    Connection con = null;
    PreparedStatement pStmt = null;
    try {
      // データベースへ接続
      con = DBManager.getConnection(); // DBManagerでコネクション

      // DELETE文を準備
      pStmt = con.prepareStatement("DELETE FROM comment WHERE id = ?"); // 検索文用意飛んできたもので調べるため?を使う
      // DELETEを実行し、実行出来た数を表示
      pStmt.setInt(1, commentId);
      pStmt.executeUpdate();

      System.out.println("delete has been completed");
      pStmt.close();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (con != null) {
        try {
          con.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public void deleteAll(int threadId) {
    Connection con = null;
    PreparedStatement pStmt = null;
    try {
      // データベースへ接続
      con = DBManager.getConnection(); // DBManagerでコネクション

      // DELETE文を準備
      pStmt = con.prepareStatement("DELETE FROM comment WHERE thread_id = ?");
      // DELETEを実行し、実行出来た数を表示
      pStmt.setInt(1, threadId);
      pStmt.executeUpdate();

      System.out.println("delete has been completed");
      pStmt.close();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (con != null) {
        try {
          con.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }
}
