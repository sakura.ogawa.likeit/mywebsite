package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.UserDataBeans;

/**
 * ユーザテーブル用のDao
 *
 *
 */
public class UserDao {

  /**
   * ログインIDとパスワードに紐づくユーザ情報を返す
   * 
   * @param loginId
   * @param password
   * @return
   */
  public UserDataBeans getUserData(String loginId, String password) {
    Connection con = null;
    PreparedStatement st = null;
    try {
      con = DBManager.getConnection();

      st = con.prepareStatement("SELECT * FROM user WHERE login_id = ? and password = ?");
      st.setString(1, loginId);
      st.setString(2, password);
      ResultSet rs = st.executeQuery();

      // レコードは1件のみなので、!rs.next()は1回だけ行う
      if (!rs.next()) {
        return null;
      }

      UserDataBeans user = new UserDataBeans();
        user.setId(rs.getInt("id"));
        user.setLoginId(rs.getString("login_id"));
        user.setName(rs.getString("name"));
        user.setPassword(rs.getString("password"));
        user.setAdmin(rs.getBoolean("is_admin"));
        user.setCreateDate(rs.getTimestamp("create_date"));

      System.out.println("login succeeded");
      return user;
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      if (con != null) {
        try {
          con.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
}

public boolean existUser(String loginId) {
  Connection conn = null;
  try {
    // データベースへ接続
    conn = DBManager.getConnection();

    String sql = "SELECT * FROM user WHERE login_id = ?";

    PreparedStatement pStmt = conn.prepareStatement(sql);
    pStmt.setString(1, loginId);
    ResultSet rs = pStmt.executeQuery();

    if (rs.next()) {
      return true;
    }

    return false;

  } catch (SQLException e) {
    e.printStackTrace();
    return false;
  } finally {
    // データベース切断
    if (conn != null) {
      try {
        conn.close();
      } catch (SQLException e) {
        e.printStackTrace();
        return false;
      }
    }
  }
}

public void insert(String loginId, String name, String password) { // LoginServletから引数をもらう
  Connection con = null;
  PreparedStatement pStmt = null;
  try {
    // データベースへ接続
    con = DBManager.getConnection(); // DBManagerでコネクション

    // INSERT文を準備
    pStmt = con.prepareStatement(
        "INSERT INTO user(login_id,name,password,create_date)VALUES(?,?,?,now())"); // 検索文用意飛んできたもので調べるため?を使う

    // INSERTを実行し、実行出来た数を表示
    pStmt.setString(1, loginId);
    pStmt.setString(2, name);
    pStmt.setString(3, password);
    pStmt.executeUpdate();
    System.out.println("inserting user has been completed");
  } catch (SQLException e) {
    e.printStackTrace();
  } finally {
    // データベース切断
    if (con != null) {
      try {
        con.close();
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
  }
}

public void delete(int userId) {
  Connection con = null;
  PreparedStatement pStmt = null;
  try {
    // データベースへ接続
    con = DBManager.getConnection(); // DBManagerでコネクション

    // DELETE文を準備
    pStmt = con.prepareStatement("DELETE FROM user WHERE id = ?");
    // DELETEを実行し、実行出来た数を表示
    pStmt.setInt(1, userId);
    pStmt.executeUpdate();

    System.out.println("delete has been completed");
    pStmt.close();

  } catch (SQLException e) {
    e.printStackTrace();
  } finally {
    // データベース切断
    if (con != null) {
      try {
        con.close();
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
  }
}

/**
 * ユーザー情報の更新処理を行う。
 *
 * 
 *
 */
public void update(String loginId, String name, String password) {
  Connection con = null;
  PreparedStatement pStmt = null;
  try {
    // データベースへ接続
    con = DBManager.getConnection();

    // UPDATE文を準備
    pStmt =
        con.prepareStatement("UPDATE user SET login_id=?, name=?, password=? WHERE login_id=?;");
    pStmt.setString(1, loginId);
    pStmt.setString(2, name);
    pStmt.setString(3, password);
    pStmt.setString(4, loginId);
    pStmt.executeUpdate();

    System.out.println("update has been completed");
    pStmt.close();

  } catch (SQLException e) {
    e.printStackTrace();
  } finally {
    try {
      con.close();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }
}
}
