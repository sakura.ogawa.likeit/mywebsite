package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.ThreadDataBeans;


public class ThreadDao {

  public List<ThreadDataBeans> findAll() {
    Connection con = null;
    PreparedStatement pStmt = null;

    List<ThreadDataBeans> threadList = new ArrayList<ThreadDataBeans>();

    try {
      // データベースへ接続
      con = DBManager.getConnection();

      pStmt = con.prepareStatement(
          "SELECT thread.id, thread.user_id, thread.user_name, thread.title, thread.overview, thread.create_date, COUNT(comment.id) AS comment_count"
              + " FROM thread" + " LEFT JOIN comment ON thread.id = comment.thread_id"
              + " GROUP BY thread.id" + " ORDER BY thread.create_date DESC;");

      // SELECTを実行し、結果表を取得
      ResultSet rs = pStmt.executeQuery();
      
      // 結果表に格納されたレコードの内容を
      // Threadインスタンスに設定し、Listに追加
      
      while (rs.next()) {
        ThreadDataBeans thread =
            new ThreadDataBeans();
        thread.setId(rs.getInt("thread.id"));
        thread.setUserId(rs.getInt("thread.user_id"));
        thread.setUserName(rs.getString("thread.user_name"));
        thread.setTitle(rs.getString("thread.title"));
        thread.setOverview(rs.getString("thread.overview"));
        thread.setCreateDate(rs.getTimestamp("thread.create_date"));
        thread.setMaxCommentId(rs.getInt("comment_count"));
        threadList.add(thread);
      }

      System.out.println("searching threadList has been completed");
      return threadList;

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (con != null) {
        try {
          con.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  public List<ThreadDataBeans> findPopular() {
    Connection con = null;
    PreparedStatement pStmt = null;

    List<ThreadDataBeans> threadList = new ArrayList<ThreadDataBeans>();

    try {
      // データベースへ接続
      con = DBManager.getConnection();

      pStmt = con.prepareStatement(
          "SELECT thread.id, thread.user_id, thread.user_name, thread.title, thread.overview, thread.create_date, COUNT(comment.id) AS comment_count"
              + " FROM thread" + " LEFT JOIN comment ON thread.id = comment.thread_id"
              + " GROUP BY thread.id" + " ORDER BY comment_count DESC;");

      // SELECTを実行し、結果表を取得
      ResultSet rs = pStmt.executeQuery();

      // 結果表に格納されたレコードの内容を
      // Threadインスタンスに設定し、Listに追加

      while (rs.next()) {
        ThreadDataBeans thread = new ThreadDataBeans();
        thread.setId(rs.getInt("thread.id"));
        thread.setUserId(rs.getInt("thread.user_id"));
        thread.setUserName(rs.getString("thread.user_name"));
        thread.setTitle(rs.getString("thread.title"));
        thread.setOverview(rs.getString("thread.overview"));
        thread.setCreateDate(rs.getTimestamp("thread.create_date"));
        thread.setMaxCommentId(rs.getInt("comment_count"));
        threadList.add(thread);
      }

      System.out.println("searching threadList has been completed");
      return threadList;

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (con != null) {
        try {
          con.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  public List<ThreadDataBeans> search(String title) {
    Connection con = null;
    PreparedStatement pStmt = null;

    List<ThreadDataBeans> threadList = new ArrayList<ThreadDataBeans>();

    try {
      // データベースへ接続
      con = DBManager.getConnection();

      pStmt = con.prepareStatement(
          "SELECT thread.id, thread.user_id, thread.user_name, thread.title, thread.overview, thread.create_date, COUNT(comment.id) AS comment_count"
              + " FROM thread LEFT JOIN comment ON thread.id = comment.thread_id"
              + " WHERE thread.title Like ?" + " GROUP BY thread.id"
              + " ORDER BY thread.create_date ASC");

      // SELECTを実行し、結果表を取得
      pStmt.setString(1, "%" + title + "%");
      ResultSet rs = pStmt.executeQuery();

      // 結果表に格納されたレコードの内容を
      // Threadインスタンスに設定し、Listに追加

      while (rs.next()) {
        ThreadDataBeans thread = new ThreadDataBeans();
        thread.setId(rs.getInt("thread.id"));
        thread.setUserId(rs.getInt("thread.user_id"));
        thread.setUserName(rs.getString("thread.user_name"));
        thread.setTitle(rs.getString("thread.title"));
        thread.setOverview(rs.getString("thread.overview"));
        thread.setCreateDate(rs.getTimestamp("thread.create_date"));
        thread.setMaxCommentId(rs.getInt("comment_count"));
        threadList.add(thread);
      }

      System.out.println("searching threadList has been completed");
      return threadList;

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (con != null) {
        try {
          con.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

public void insert(int userId, String userName, String title, String overview) {
  Connection con = null;
  PreparedStatement pStmt = null;
  try {
    // データベースへ接続
    con = DBManager.getConnection(); // DBManagerでコネクション

    // INSERT文を準備
    pStmt = con.prepareStatement(
        "INSERT INTO thread(user_id,user_name,title,overview,create_date)VALUES(?,?,?,?,now())"); // 検索文用意飛んできたもので調べるため?を使う

    // INSERTを実行し、実行出来た数を表示
    pStmt.setInt(1, userId);
    pStmt.setString(2, userName);
    pStmt.setString(3, title);
    pStmt.setString(4, overview);
    pStmt.executeUpdate();
    System.out.println("inserting thread has been completed");
    pStmt.close();
  } catch (SQLException e) {
    e.printStackTrace();
  } finally {
    // データベース切断
    if (con != null) {
      try {
        con.close();
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
  }
}

public ThreadDataBeans findThreadData(int threadId) {
  Connection con = null;
  PreparedStatement pStmt = null;
  try {
    // データベースへ接続
    con = DBManager.getConnection(); // DBManagerでコネクション

    // SELECT文を準備
    pStmt = con.prepareStatement(
        "SELECT * FROM thread WHERE id = ?"); // 検索文用意飛んできたもので調べるため?を使う

    // SELECTを実行し、結果表を取得
    pStmt.setInt(1, threadId);
    ResultSet rs = pStmt.executeQuery();

    // レコードは1件のみなので、rs.next()は1回だけ行う
    ThreadDataBeans thread = new ThreadDataBeans();
    if (rs.next()) {
      thread.setId(rs.getInt("id"));
      thread.setUserId(rs.getInt("user_id"));
      thread.setUserName(rs.getString("user_name"));
      thread.setTitle(rs.getString("title"));
      thread.setOverview(rs.getString("overview"));
      thread.setCreateDate(rs.getTimestamp("create_date"));
    }
    System.out.println("searching thread has been completed");
    return thread;

  } catch (SQLException e) {
    e.printStackTrace();
    return null;
  } finally {
    // データベース切断
    if (con != null) {
      try {
        con.close();
      } catch (SQLException e) {
        e.printStackTrace();
        return null;
      }
    }
  }
}

public void delete(int threadId) {
  Connection con = null;
  PreparedStatement pStmt = null;
  try {
    // データベースへ接続
    con = DBManager.getConnection(); // DBManagerでコネクション

    // DELETE文を準備
    pStmt = con.prepareStatement("DELETE FROM thread WHERE id = ?"); // 検索文用意飛んできたもので調べるため?を使う
    // DELETEを実行し、実行出来た数を表示
    pStmt.setInt(1, threadId);
    pStmt.executeUpdate();

    System.out.println("delete has been completed");
    pStmt.close();

  } catch (SQLException e) {
    e.printStackTrace();
  } finally {
    // データベース切断
    if (con != null) {
      try {
        con.close();
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
  }
}

}
