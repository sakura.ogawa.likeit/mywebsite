package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.ThreadDataBeans;

public class FavoriteDao {
  public List<ThreadDataBeans> getFavoriteData(int userId) {
    Connection con = null;
    PreparedStatement pStmt = null;
    List<ThreadDataBeans> threadList = new ArrayList<ThreadDataBeans>();

    try {
      // データベースへ接続
      con = DBManager.getConnection(); // DBManagerでコネクション

      // SELECTを実行し、結果表を取得
      pStmt = con.prepareStatement(
          "SELECT * FROM favorite JOIN thread ON favorite.thread_id = thread.id WHERE favorite.user_id = ? ORDER BY create_date ASC");
      pStmt.setInt(1, userId);

      ResultSet rs = pStmt.executeQuery();

      // 結果表に格納されたレコードの内容を
      // Threadインスタンスに設定し、Listに追加
      while (rs.next()) {
        ThreadDataBeans thread = new ThreadDataBeans();
        int id = rs.getInt("id");
        thread.setUserId(rs.getInt("user_id"));
        thread.setUserName(rs.getString("user_name"));
        thread.setTitle(rs.getString("title"));
        thread.setOverview(rs.getString("overview"));
        thread.setCreateDate(rs.getTimestamp("create_date"));
        thread.setId(rs.getInt("id"));
        threadList.add(thread);
      }
      System.out.println("searching threadList has been completed");
      return threadList;

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (con != null) {
        try {
          con.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  public void change(int userId, int threadId) {
    Connection con = null;
    PreparedStatement pStmt = null;

    try {
      // データベースへ接続
      con = DBManager.getConnection(); // DBManagerでコネクション

      // SELECTを実行し、結果表を取得
      pStmt = con
          .prepareStatement("SELECT thread_id FROM favorite WHERE user_id = ? and thread_id = ?");
      pStmt.setInt(1, userId);
      pStmt.setInt(2, threadId);
      ResultSet rs = pStmt.executeQuery();

      if (rs.next()) {
        pStmt = con.prepareStatement("DELETE FROM favorite WHERE user_id = ? and thread_id = ?");
        pStmt.setInt(1, userId);
        pStmt.setInt(2, threadId);
        pStmt.executeUpdate();

        System.out.println("delete has been completed");
      } else {
        pStmt = con.prepareStatement("INSERT INTO favorite(user_id, thread_id)VALUES(?,?)");
        pStmt.setInt(1, userId);
        pStmt.setInt(2, threadId);
        pStmt.executeUpdate();
        System.out.println("inserting favorite has been completed");
      }

    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      // データベース切断
      if (pStmt != null) {
        try {
          pStmt.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
      if (con != null) {
        try {
          con.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public void delete(int userId) {
    Connection con = null;
    PreparedStatement pStmt = null;
    try {
      // データベースへ接続
      con = DBManager.getConnection(); // DBManagerでコネクション

      // DELETE文を準備
      pStmt = con.prepareStatement("DELETE FROM favorite WHERE user_id = ?"); // 検索文用意飛んできたもので調べるため?を使う
      // DELETEを実行し、実行出来た数を表示
      pStmt.setInt(1, userId);
      pStmt.executeUpdate();

      System.out.println("delete has been completed");
      pStmt.close();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (con != null) {
        try {
          con.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }
}
