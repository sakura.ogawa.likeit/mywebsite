package model;

import java.io.Serializable;

public class FavoriteDataBeans implements Serializable {
  private int userId;
  private int threadId;




  public int getUserId() {
    return userId;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

  public int getThreadId() {
    return threadId;
  }

  public void setThreadId(int threadId) {
    this.threadId = threadId;
  }


}
