package model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * スレッド
 *
 */
public class ThreadDataBeans implements Serializable {
  private int id;
  private int userId;
  private String userName;
  private String title;
  private String overview;
  private Date createDate;
  
  private int MaxCommentId;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

 
  public int getUserId() {
    return userId;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getOverview() {
    return overview;
  }

  public void setOverview(String overview) {
    this.overview = overview;
  }


  public Date getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Date createDate) {
    this.createDate = createDate;
  }


  public String getFormatDate() {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日HH時mm分");
    return sdf.format(createDate);
  }

  public int getMaxCommentId() {
    return MaxCommentId;
  }

  public void setMaxCommentId(int maxCommentId) {
    MaxCommentId = maxCommentId;
  }




}
