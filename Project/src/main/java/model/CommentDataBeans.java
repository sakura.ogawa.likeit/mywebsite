package model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * コメント
 *
 */
public class CommentDataBeans implements Serializable {
  private int id;
  private int NewId;;
  private int threadId;
  private int userId;
  private String userName;
  private String comment;
  private Date createDate;
  
  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }

  public int getNewId() {
    return NewId;
  }

  public void setNewId(int newId) {
    NewId = newId;
  }
  public int getThreadId() {
    return threadId;
  }
  public void setThreadId(int threadId) {
    this.threadId = threadId;
  }
  public int getUserId() {
    return userId;
  }
  public void setUserId(int userId) {
    this.userId = userId;
  }
  public String getUserName() {
    return userName;
  }
  public void setUserName(String userName) {
    this.userName = userName;
  }
  public String getComment() {
    return comment;
  }
  public void setComment(String comment) {
    this.comment = comment;
  }
  public Date getCreateDate() {
    return createDate;
  }
  public void setCreateDate(Date createDate) {
    this.createDate = createDate;
  }

  public String getFormatDate() {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日HH時mm分");
    return sdf.format(createDate);
  }

}
