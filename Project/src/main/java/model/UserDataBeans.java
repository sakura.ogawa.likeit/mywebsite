package model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * ユーザー
 *
 */
public class UserDataBeans implements Serializable {
  private int id;
  private String name;
  private String loginId;
  private String password;
  private boolean isAdmin;
  private Date createDate;

  public UserDataBeans() {}

  // 全てのデータをセットするコンストラクタ
  public UserDataBeans(int id, String loginId, String name, String password, boolean isAdimn,
      Timestamp createDate) {
    this.id = id;
    this.loginId = loginId;
    this.name = name;
    this.password = password;
    this.isAdmin = isAdimn;
    this.createDate = createDate;
  }
  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLoginId() {
    return loginId;
  }

  public void setLoginId(String loginId) {
    this.loginId = loginId;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public boolean isAdmin() {
    return isAdmin;
  }


  public void setAdmin(boolean isAdmin) {
    this.isAdmin = isAdmin;
  }

  public Date getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Date createDate) {
    this.createDate = createDate;
  }

  public String getFormatDate() {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日HH時mm分");
    return sdf.format(createDate);
  }

}
