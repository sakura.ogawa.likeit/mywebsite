<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>○○掲示板 | 検索結果一覧</title>
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<style>
/* ヘッダーの余白 */
header {
	margin-bottom: 20px;
}
/*ヘッダーの色*/
.header-one {
	background-color: #4682b4;
}
.btn-ellipse {
    border-radius: 60% / 50%;
  }

</style>
</head>

<body>
	<!-- ヘッダー -->
	<header>
		<nav
			class="navbar navbar-dark navbar-expand  flex-md-row justify-content-end header-one">
			<ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
				<li class="nav-item active">
					<!-- ページタイトル、スレッド一覧へのリンク --> <a class="nav-link"
					href="ThreadListServlet">○○掲示板</a>
				</li>
			</ul>
			<div class="col-9 text-center">
				<form method="post" action="SearchResultServlet">
					<input type="text" class="headerSearch" placeholder="スレッドを探す"
						name="title" value="">
					<button type="submit" class="btn btn-primary btn-sm form-submit"
						style="position: relative; top: -2px;">検索</button>
				</form>
			</div>

			<ul class="navbar-nav flex-row">
				<!-- ログインユーザー名表示、更新ページへのリンク -->
				<li class="nav-item"><a class="nav-link"
					href="UserDetailServlet">${userInfo.name}さん</a></li>
				<!-- ログアウトリンク -->
				<li class="nav-item"><a class="nav-link" href="LogoutServlet">ログアウト</a>
				</li>
			</ul>
		</nav>
	</header>

	<div class="container">
		<div class="row">
			<div class="col">
				<h1 class="text-center">○○掲示板</h1>
			</div>
		</div>


			<div class="col-6 offset-3">
				<!-- エラーメッセージ start -->
				<c:if test="${errMsg != null}">
					<div class="alert alert-primary" role="alert">${errMsg}</div>
				</c:if>
				<!--エラーメッセージ  end  -->
			</div>
	</div>

	<div class="row">
		<div class="col">
			<div class="card">
				<table class="table">
					<thead class="thead-light">
						<tr>
							<th>タイトル</th>
							<th>コメント数</th>
							<th>更新日</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<!-- スレッド表示用 for文 -->
						<c:forEach var="thread" items="${threadList}">
							<tr>
								<td class="col-9">
									<!-- スレッド表示・コメント投稿ページ リンク --> <a
									href="ThreadCommentServlet?id=${thread.id}"
									class="text-primary">${thread.title}</a>
								</td>
								<td class="col-2">${thread.maxCommentId}</td>
								<!-- スレッド表示・コメント投稿ページ リンク -->
								<td>${thread.createDate}</td>
								<td class="text-right col-1">
									<!-- スレッド削除ボタン 表示制御 --> <c:if
										test="${userInfo.id == thread.userId}">
										<!-- スレッド削除ボタン -->
										<a type="button" class="btn btn-danger"
											href="ThreadCommentDeleteServlet?id=${thread.id}">削除</a>
									</c:if>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>

			</div>
		</div>
	</div>
	<div class="row mt-3">
            <div class="col">
                <!-- スレッド一覧に戻るリンク -->
                <a href="ThreadListServlet" class="text-primary">戻る</a>
            </div>
        </div>
	
</body>

</html>