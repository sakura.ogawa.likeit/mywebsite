<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>○○掲示板 | スレッドコメント一覧</title>
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <!-- BootstrapのCSS読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>

<body>
    <!-- ヘッダー -->
    <header>
        <nav class="navbar navbar-dark navbar-expand  flex-md-row justify-content-end header-one">
            <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
                <li class="nav-item active">
                    <!-- ページタイトル、スレッド一覧へのリンク -->
                    <a class="nav-link" href="ThreadListServlet">○○掲示板</a>
                </li>
            </ul>
            <ul class="navbar-nav flex-row">
                <!-- ログインユーザー名表示、更新ページへのリンク -->
                <li class="nav-item"><a class="nav-link" href="UserDetailServlet">${userInfo.name}さん</a></li>
                <!-- ログアウトリンク -->
                <li class="nav-item"><a class="nav-link" href="LogoutServlet">ログアウト</a>
                </li>
            </ul>
        </nav>
    </header>

    <div class="container">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <!--スレッドタイトル-->
                        <h2 class="text-center">${thread.title}</h2>
                        <div class="text-right">
                            <!-- お気に入り登録ボタン -->
                            <form action="ThreadCommentServlet" method="POST">
                                <input type="hidden" name="thread-id" value="${thread.id}">
                                <button type="submit" class="btn btn-primary" data-toggle="button" aria-pressed="false" autocomplete="off">☆</button>
                            </form>
                            <!-- スレッド削除ボタンの表示制御-->
                            <c:if test="${userInfo.isAdmin()}">
                                <!-- スレッド削除ページ リンク -->
                                <a type="button" class="btn btn-danger"
                                    href="ThreadCommentDeleteServlet?id=${thread.id}">スレッドを削除する</a>
                             </c:if>
                        </div>
                    </div>
                    <div class="card-body">
                        <p class="card-text">
                            <!--スレッド本文-->
                            ${thread.overview}
                        </p>

                        <div class="d-flex justify-content-between">
                            <!--スレッド投稿者、時間-->
                            <p class="card-text text-muted">名前 : ${thread.userName}</p>
                            <p class="card-text text-muted" style="margin-right: 600px">ID : ${thread.userId}</p>
                            <p class="card-text text-muted">${thread.createDate}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-10 offset-2">
                <!-- コメント一覧表示用 for文 -->
                <c:forEach var="comment" items="${commentList}">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex justify-content-between">
                                <!--コメント番号、返信数、コメント投稿者、時間-->
                                <p class="card-text text-muted">${comment.newId}</p>
                                 <!--返信数ボタン表示の制御-->
                            <c:if test="${reply-comment.id != null}">
                                <!--返信コメントページへのリンク-->
                                <p class="card-text text-muted">
                                    <a href="ReplyCommentServlet?id=${comment.id}"
                                        class="text-primary">${reply-comment.id}</a>
                                </p>
                            </c:if>
                                <!--ユーザーコメントページへのリンク-->
                                <p class="card-text text-muted">
                                    <!-- スレッド表示・コメント投稿ページ リンク -->
                                    <a href="UserCommentServlet?threadId=${thread.id}&userId=${comment.userId}"
                                        class="text-primary">ID : ${comment.userId}</a>
                                </p>
                                <p class="card-text text-muted">名前 : ${comment.userName}</p>
                                <p class="card-text text-muted">${comment.createDate}</p>
                            </div>
                            <p class="card-text">
                                <!--コメント本文-->
                                ${comment.comment}
                            </p>
                            <div class="text-right">
                                <!--返信投稿ボタン-->
                            <a type="button" class="btn btn-success"
                                href="ReplyCommentServlet?id=${comment.id}">返信</a>
                            </div>
                            <!--コメント削除ボタン表示の制御-->
                            <c:if test="${userInfo.id == comment.userId || userInfo.isAdmin()}">
                                <div class="text-right">
                                    <!--コメント削除ページへのリンク-->
                                    <a type="button" class="btn btn-danger"
                                        href="CommentDeleteServlet?threadId=${thread.id}&commentId=${comment.id}">削除する</a>
                                </div>
                            </c:if>
                            
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>

        <!-- コメント投稿エリア -->
        <div class="row mt-3">
            <div class="col">

                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <!-- コメント投稿時のエラーメッセージ start -->
                                <c:if test="${errMsg != null}">
                                    <div class="alert alert-danger" role="alert">${errMsg}</div>
                                </c:if>
                                <!-- コメント投稿時のエラーメッセージ  end  -->
                            </div>
                        </div>
                        <!-- コメント投稿フォーム start -->
                        <form method="post" action="CommentAddServlet">
                            <!-- コメント投稿に必要なスレッドIDをhiddenでもたせる -->
                            <input type="hidden" name="thread-id" value="${thread.id}">
                            <div class="form-group row">
                                <div class="col-10">
                                    <!-- コメント本文 入力 -->
                                    <textarea class="form-control" id="comment" name="comment"
                                        rows="3">${comment}</textarea>
                                </div>
                                <div class="col-2">
                                    <button type="submit" class="btn btn-primary btn-lg btn-block">コメント<br>投稿</button>
                                </div>
                            </div>
                        </form>
                        <!-- コメント投稿フォーム end -->
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col">
                <!-- スレッド一覧に戻るリンク -->
                <a href="ThreadListServlet" class="text-primary">戻る</a>
            </div>
        </div>

</body>

</html>