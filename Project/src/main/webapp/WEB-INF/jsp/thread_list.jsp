<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- DataFormat用 -->
		<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>○○掲示板 | スレッド一覧</title>
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<style>
/* ヘッダーの余白 */
header {
	margin-bottom: 20px;
}
/*ヘッダーの色*/
.header-one {
	background-color: #4682b4;
}
.btn-ellipse {
    border-radius: 60% / 50%;
  }

</style>
</head>

<body>
	<!-- ヘッダー -->
	<header>
		<nav
			class="navbar navbar-dark navbar-expand  flex-md-row justify-content-end header-one">
			<ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
				<li class="nav-item active">
					<!-- ページタイトル、スレッド一覧へのリンク --> <a class="nav-link"
					href="ThreadListServlet">○○掲示板</a>
				</li>
			</ul>
			<div class="col-9 text-center">
				<form method="post" action="SearchResultServlet">
					<input type="text" class="headerSearch" placeholder="スレッドを探す"
						name="title" value="">
					<button type="submit" class="btn btn-primary btn-sm form-submit"
						style="position: relative; top: -2px;">検索</button>
				</form>
			</div>

			<ul class="navbar-nav flex-row">
				<!-- ログインユーザー名表示、更新ページへのリンク -->
				<li class="nav-item"><a class="nav-link"
					href="UserDetailServlet">${userInfo.name}さん</a></li>
				<!-- ログアウトリンク -->
				<li class="nav-item"><a class="nav-link" href="LogoutServlet">ログアウト</a>
				</li>
			</ul>
		</nav>
	</header>

	<div class="container">
		<div class="row">
			<div class="col">
				<h1 class="text-center">○○掲示板</h1>
			</div>
		</div>


		<div class="row">
			<div class="col mt-3 d-flex justify-content-between">
				<h3 class="text-center">スレッド一覧</h3>

				<form action="ThreadListServlet" method="POST">
					<div class="row">
						<div class="col">
							<button type="submit"
								class="btn btn-primary btn-sm btn-ellipse form-submit"
								name="change_button" value="new">
								新着スレッド順</a>
						</div>
						<div class="col">
							<button type="submit"
								class="btn btn-primary btn-sm btn-ellipse form-submit"
								name="change_button" value="popular">
								人気スレッド順</a>
						</div>
					</div>
				</form>
				<!-- スレッド追加リンク-->
				<div class="row">
					<div class="col-">
						<a href="ThreadAddServlet"
							class="btn btn-outline-success btn-sm form-submit">スレッドを立てる</a>
					</div>
					<!-- お気に入りリンク-->
					<div class="col">
						<a href="FavoriteThreadServlet"
							class="btn btn-outline-primary btn-sm form-submit">お気に入り</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col">
			<div class="card">
				<table class="table">
					<thead class="thead-light">
						<tr>
							<th>タイトル</th>
							<th>コメント数</th>
							<th>更新日</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<!-- スレッド表示用 for文 -->
						<c:forEach var="thread" items="${threadList}">
							<tr>
								<td class="col-8">
									<!-- スレッド表示・コメント投稿ページ リンク --> <a
									href="ThreadCommentServlet?id=${thread.id}"
									class="text-primary">${thread.title}</a>
								</td>
								<td class="col-2">${thread.maxCommentId}</td>
								<!-- スレッド表示・コメント投稿ページ リンク -->
								<td class="col-4">
								<fmt:formatDate value="${thread.createDate}" pattern="yyyy年MM月dd日H:mm"/>
								</td>
								<td class="text-right col-1">
									<!-- スレッド削除ボタン 表示制御 --> 
									<c:if test="${userInfo.isAdmin()}">
										<!-- スレッド削除ボタン -->
										<a type="button" class="btn btn-danger"
											href="ThreadCommentDeleteServlet?id=${thread.id}">削除</a>
									</c:if>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>

			</div>
		</div>
	</div>
</body>

</html>