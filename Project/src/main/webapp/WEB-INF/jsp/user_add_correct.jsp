<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>新規登録</title>
<!-- header.cssの読み込み -->
<link href="css/header.css" rel="stylesheet" type="text/css" />
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<header>
		<nav
			class="navbar navbar-dark navbar-expand flex-md-row justify-content-end header-one">
			<ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
				<li class="nav-item active">
					<!-- ページタイトル、スレッド一覧へのリンク --> <a class="nav-link"
					href="LoginServlet">○○掲示板</a>
				</li>
			</ul>
		</nav>
	</header>


	<div class="container mt-5">
		<div class="row">
			<div class="col s12 light">
				<h3 class="text-center">新規登録</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-6 offset-3">
				<!-- エラーメッセージ start -->
				<c:if test="${errMsg != null}">
					<div class="alert alert-danger" role="alert">${errMsg}</div>
				</c:if>
				<!--エラーメッセージ  end  -->
			</div>
		</div>

		<div class="row">
			<div class="col-sm-6 offset-sm-3">
				<div class="card bg-light mb-3">
					<div class="card-content">
						<!--   ユーザー登録フォーム   -->
						<form action="UserAddServlet" method="POST">
							<div class="form-group row mt-1"></div>
							<div class="form-group row">
								<label for="user-id" class="control-label col-3">ログインID</label>
								<div class="col-8">
									<!-- ログインID 入力 -->
									<input type="text" class="form-control" id="loginId"
										name="loginId" value="${loginId}">
								</div>
							</div>
							<div class="form-group row">
								<label for="password" class="control-label col-3">パスワード</label>
								<div class="col-8">
									<!-- パスワード 入力 -->
									<input type="password" class="form-control" id="password"
										name="password">
								</div>
							</div>
							<div class="form-group row">
								<label for="password-confirm" class="control-label col-3">パスワード<br>(確認)
								</label>
								<div class="col-8">
									<!-- パスワード 入力 -->
									<input type="password" class="form-control"
										id="password-confirm" name="password-confirm">
								</div>
							</div>
							<div class="form-group row">
								<label for="user-name" class="control-label col-3">ユーザー名</label>
								<div class="col-8">
									<!-- ユーザー名 入力 -->
									<input type="text" class="form-control" id="user-name"
										name="user-name" value="${userName}">
								</div>
							</div>

							<div>
								<button type="submit"
									class="btn btn-primary btn-block form-submit">確認</button>
							</div>
						</form>
					</div>
				</div>
				<div class="row mt-3">
					<div class="col">
						<!-- 戻るボタン-->
						<a href="LoginServlet" class="text-primary">戻る</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>

</html>