<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>○○掲示板 | ログイン</title
<!-- Bootstrapの読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<style>
/* ヘッダーの余白 */
header {
	margin-bottom: 20px;
}
/*ヘッダーの色*/
.header-one {
	background-color: #4682b4;
}
</style>
</head>

<body>
	<!-- ヘッダー -->
	<header>
		<nav class="navbar navbar-dark navbar-expand flex-md-row header-one">
			<ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
				<li class="nav-item active">
					<!-- ページタイトル、スレッド一覧へのリンク --> <a class="nav-link"
					href="LoginServlet">○○掲示板</a>
				</li>
			</ul>
			<ul class="navbar-nav flex-row">
				<li class="nav-item">
					<!-- ユーザ登録ページへのリンク --> <a class="nav-link" href="UserAddServlet">新規登録</a>
				</li>
			</ul>
		</nav>
	</header>

	<div class="container">
		<div class="row">
			<div class="col">
				<h3 class="text-center">ログイン</h3>
			</div>
		</div>

		<div class="row">
			<div class="col-6 offset-3">
				<!-- エラーメッセージ start -->
				<c:if test="${errMsg != null}">
					<div class="alert alert-danger" role="alert">${errMsg}</div>
				</c:if>
				<!--エラーメッセージ  end  -->
			</div>
		</div>

		<div class="row">
			<div class="col-6 offset-sm-3">
				<div class="card bg-light mb-3">
					<div class="card-content">
						<!--   ログインフォーム   -->
						<form method="post" action="LoginServlet">
							<div class="form-group row mt-1"></div>
							<div class="form-group row">
								<label for="user-id" class="control-label col-3">ログインID</label>
								<div class="col-8">
									<!--ログインID入力 -->
									<input type="text" name="loginId" id="loginId"
										class="form-control" value="${loginId}">
								</div>
							</div>

							<div class="form-group row">
								<label for="password" class="control-label col-3">パスワード</label>
								<div class="col-8">
									<!--パスワード入力-->
									<input type="password" name="password" id="password"
										class="form-control">
								</div>
							</div>

							<div class="row mt-4"></div>
							<div>
								<button type="submit"
									class="btn btn-primary btn-block form-submit">ログイン</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>

</html>