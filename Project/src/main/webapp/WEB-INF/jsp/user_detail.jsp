<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
		<!-- DataFormat用 -->
		<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

			<!DOCTYPE html>
			<html lang="ja">

			<head>
				<meta charset="UTF-8">
				<meta name="viewport" content="width=device-width, initial-scale=1">
				<title>○○掲示板 | ユーザー情報詳細</title>
				<!-- header.cssの読み込み -->
				<link href="css/header.css" rel="stylesheet" type="text/css" />
				<!-- BootstrapのCSS読み込み -->
				<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
					integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
					crossorigin="anonymous">
				<!-- home.cssの読み込み -->
				<link href="css/home.css" rel="stylesheet" type="text/css" />
			</head>

			<body>

				<!-- ヘッダー -->
				<header>
					<nav class="navbar navbar-dark navbar-expand  flex-md-row justify-content-end header-one">
						<ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
							<li class="nav-item active">
								<!-- ページタイトル、スレッド一覧へのリンク -->
								<a class="nav-link" href="ThreadListServlet">○○掲示板</a>
							</li>
						</ul>
						<ul class="navbar-nav flex-row">
							<!-- ログインユーザー名表示、更新ページへのリンク -->
							<li class="nav-item"><a class="nav-link" href="UserDetailServlet">${userInfo.name}さん</a></li>
							<!-- ログアウトリンク -->
							<li class="nav-item"><a class="nav-link" href="LogoutServlet">ログアウト</a>
							</li>
						</ul>
					</nav>
				</header>

				<div class="container">
					<div class="row">
						<div class="col">
							<h3 class="text-center">ユーザ情報詳細</h3>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-6 offset-sm-3">
							<div class="card bg-light mb-3">
								<div class="card-content">

									<div class="row">
										<label for="loginId" class="col-3 font-weight-bold">ログインID</label>
										<div class="col-9">
											<p>${userInfo.loginId}</p>
										</div>
									</div>

									<div class="row">
										<label for="userName" class="col-3 font-weight-bold">ユーザ名</label>
										<div class="col-9">
											<p>${userInfo.name}</p>
										</div>
									</div>

									<div class="row">
										<label for="createDate" class="col-3 font-weight-bold">新規登録日時</label>
										<div class="col-9">
											<p>
												<fmt:formatDate value="${userInfo.createDate}" pattern="yyyy年MM月dd日H:mm" />
											</p>
										</div>
									</div>

									<div class="row mt-3">
										<div class="col">
											<a href="UserUpdateServlet?id=${user.id}" class="btn btn-success btn-block form-submit">更新</a>
										</div>
										<div class="col">
											<a href="UserDeleteServlet?id=${user.id}" class="btn btn-danger btn-block form-submit">削除</a>
										</div>
									</div>
								</div>
							</div>
							<div class="row mt-3">
								<div class="col">
									<!-- 戻るボタン-->
									<a href="ThreadListServlet" class="text-primary">戻る</a>
								</div>
							</div>
						</div>

			</body>

			</html>