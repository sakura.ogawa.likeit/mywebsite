<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title>ログアウト</title>
	<!-- header.cssの読み込み -->
	<link href="css/header.css" rel="stylesheet" type="text/css" />
	<!-- Bootstrapの読み込み -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"
		integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
	<!-- ヘッダー -->
	<header>
		<nav class="navbar navbar-dark navbar-expand flex-md-row justify-content-end header-one">
			<ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
				<li class="nav-item active">
					<!-- ページタイトル、スレッド一覧へのリンク -->
					<a class="nav-link" href="LoginServlet">○○掲示板</a>
				</li>
			</ul>
		</nav>
	</header>

	<div class="container">
		<div class="row">
			<div class="col s8 offset-s2">
				<div class="card bg-light mb-3">
					<div class="card-content">
						<div class="row">
							<div class="col s12">
								<div style="padding-top: 15px;"></div>
								<h4 class="text-center">ログアウトしました</h4>

								<div class="row mt-3">
									<div class="col s12">
										<p class="text-center">
											<a href="LoginServlet" class="btn  btn-primary btn-lg">ログインページへ</a>
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>

</html>