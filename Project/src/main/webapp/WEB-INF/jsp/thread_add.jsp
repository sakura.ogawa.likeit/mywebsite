<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>◯◯掲示板 | スレッド投稿</title>
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>

<body>
    <!-- ヘッダー -->
    <header>
        <nav class="navbar navbar-dark navbar-expand  flex-md-row justify-content-end header-one">
            <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
                <li class="nav-item active">
                    <!-- ページタイトル、スレッド一覧へのリンク -->
                    <a class="nav-link" href="ThreadListServlet">○○掲示板</a>
                </li>
            </ul>
            <ul class="navbar-nav flex-row">
                <!-- ログインユーザー名表示、更新ページへのリンク -->
                <li class="nav-item"><a class="nav-link" href="UserDetailServlet">${userInfo.name}さん</a></li>
                <!-- ログアウトリンク -->
                <li class="nav-item"><a class="nav-link" href="LogoutServlet">ログアウト</a>
                </li>
            </ul>
        </nav>
    </header>


    <div class="container mt-6">
        <div class="row">
            <div class="col- mx-auto">
                <h3>スレッド投稿</h3>
            </div>
        </div>

        <div class="row mt-2">
            <div class="col-6 offset-3">
                <!-- エラーメッセージ start -->
                <c:if test="${errMsg != null}">
                    <div class="alert alert-danger" role="alert">${errMsg}</div>
                </c:if>
                <!--エラーメッセージ  end  -->
            </div>
        </div>

        <div class="row">
            <div class="col-6 offset-sm-3">
                <div class="card bg-light mb-3">
                    <div class="card-content">
                        <!--   スレッド投稿フォーム   -->
                        <form method="post" action="ThreadAddServlet">
                            <div class="form-group row mt-1"></div>
                            <div class="form-group row">
                            <label for="user-name" class="control-label col-3">ユーザーID</label>
                                <div class="col-8">
                                    <!-- ユーザー名 -->
                                    <input type="text" name="user-id" id="user-id" class="form-control"
                                        value="${userInfo.id}" readonly>
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="user-name" class="control-label col-3">ユーザー名</label>
                                <div class="col-8">
                                    <!-- ユーザー名 表示 -->
                                    <input type="text" name="user-name" id="user-name" class="form-control"
                                        value="${userInfo.name}" readonly>
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="title" class="control-label col-3">タイトル</label>
                                <div class="col-8">
                                    <!-- スレッドのタイトル 入力 -->
                                    <input type="text" name="title" class="form-control" value="${title}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="body" class="control-label col-3">本文</label>
                                <div class="col-9">
                                    <!-- スレッドの本文 入力 -->
                                    <textarea class="form-control" id="overview" name="overview" rows="4">${overview}</textarea>
                                </div>
                            </div>
                            <div>
                                <button type="submit" class="btn btn-primary btn-block form-submit">登録</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col">
                        <!-- スレッド一覧ページに戻るリンク -->
                        <a href="ThreadListServlet" class="text-primary">戻る</a>
                    </div>
                </div>
            </div>

</body>

</html>