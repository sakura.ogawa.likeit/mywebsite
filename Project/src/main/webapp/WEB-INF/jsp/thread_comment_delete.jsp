<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>s
<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>○○掲示板 | スレッド削除確認</title>
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <!-- BootstrapのCSS読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>

<body>
    <!-- ヘッダー -->
    <header>
        <nav class="navbar navbar-dark navbar-expand  flex-md-row justify-content-end header-one">
            <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
                <li class="nav-item active">
                    <!-- ページタイトル、スレッド一覧へのリンク -->
                    <a class="nav-link" href="ThreadListServlet">○○掲示板</a>
                </li>
            </ul>
            <ul class="navbar-nav flex-row">
                <!-- ログインユーザー名表示、更新ページへのリンク -->
                <li class="nav-item"><a class="nav-link" href="UserDetailServlet">${userInfo.name}さん</a></li>
                <!-- ログアウトリンク -->
                <li class="nav-item"><a class="nav-link" href="LogoutServlet">ログアウト</a>
                </li>
            </ul>
        </nav>
    </header>

    <div class="container">
        <div class="row">
            <div class="col-6 offset-3">
                <div class="card">
                    <div class="card-header">
                        <h3 class="text-center">スレッドを削除します</h3>
                    </div>
                    <div class="card-body">
                        <h5 class="card-text text-danger">
                            スレッド名 ： ${thread.title}
                        </h5>
                        <p class="card-text">
                            このスレッドに関連する全てのコメントも同時に削除されます。
                        </p>

                        <div class="row">
                            <div class="col">
                                <!--スレッド表示・コメント投稿ページへ戻るリンク -->
                                <a href="ThreadListServlet" class="btn btn-outline-primary btn-block">戻る</a>
                            </div>
                            <div class="col">
                                <!-- スレッドとコメントを削除するフォーム -->
                                <form method="post" action="ThreadDeleteResultServlet">
                                    <!-- 削除するThreadのIDをhiddenでもたせる -->
                                    <input type="hidden" name="thread-id" value="${thread.id}">
                                    <button type="submit" class="btn btn-danger btn-block">削除する</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

</body>

</html>