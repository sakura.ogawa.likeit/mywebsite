﻿CREATE DATABASE mywebsite DEFAULT CHARACTER SET utf8;
use mywebsite;
CREATE TABLE user(
id SERIAL primary key AUTO_INCREMENT NOT NULL,
login_id varchar(255) UNIQUE NOT NULL,
name  varchar(255) NOT NULL,
password varchar(255) NOT NULL,
is_admin boolean  NOT NULL default false,
create_date DATETIME NOT NULL
);

CREATE TABLE thread(
id SERIAL primary key AUTO_INCREMENT NOT NULL,
user_id varchar(255) UNIQUE NOT NULL,
title  varchar(255) NOT NULL,
overview varchar(255) NOT NULL,
create_date DATETIME NOT NULL,
is_favorite boolean  NOT NULL default false
);

CREATE TABLE comment(
id SERIAL primary key AUTO_INCREMENT NOT NULL,
thread_id varchar(255) UNIQUE NOT NULL,
user_id varchar(255) UNIQUE NOT NULL,
comment varchar(255) NOT NULL,
create_date DATETIME NOT NULL
);

CREATE TABLE reply(
id SERIAL primary key AUTO_INCREMENT NOT NULL,
thread_id varchar(255) UNIQUE NOT NULL,
comment_id varchar(255) UNIQUE NOT NULL,
user_id varchar(255) UNIQUE NOT NULL,
user_name  varchar(255) NOT NULL,
comment varchar(255) NOT NULL,
create_date DATETIME NOT NULL
);

INSERT INTO user(login_id,name,password,is_admin,create_date) VALUES(
'admin','管理者','password',true,NOW()
);
ALTER TABLE thread ADD user_name VARCHAR(255) NOT NULL;
ALTER TABLE comment ADD user_name VARCHAR(255) NOT NULL;

ALTER TABLE thread MODIFY user_id INT NOT NULL;
ALTER TABLE comment MODIFY thread_id INT NOT NULL;
ALTER TABLE comment MODIFY user_id INT NOT NULL;
ALTER TABLE reply MODIFY thread_id INT NOT NULL;
ALTER TABLE reply MODIFY comment_id INT NOT NULL;
ALTER TABLE reply MODIFY user_id INT NOT NULL;

ALTER TABLE thread
DROP COLUMN is_favorite;

CREATE TABLE favorite(
id SERIAL primary key AUTO_INCREMENT NOT NULL,
user_id INT UNIQUE NOT NULL,
thread_id INT UNIQUE NOT NULL,
is_favorite boolean  NOT NULL default false
);

SELECT ROW_NUMBER() OVER (ORDER BY comment.create_date ASC) AS new_id, comment.id, comment.thread_id, comment.user_id, comment.comment, comment.create_date
FROM comment
JOIN reply ON comment.id = reply.comment_id
WHERE comment.thread_id = ?
ORDER BY comment.create_date ASC;

select t.* from thread t, comment c, reply r where t.id = c.thread_id and  
 t.id and r.thread_id;
 
 select t.* from thread t, comment c, reply r where t.id = c.thread_id and  
 t.id and r.thread_id Group by t.id;
 
 select sum(c.id), sum(r.id), t.* from thread t, comment c, reply r where t.id = c.thread_id and  
 t.id and r.thread_id Group by t.id;
 
 ALTER TABLE favorite
DROP COLUMN is_favorite;

SELECT thread.id, thread.user_id, thread.user_name, thread.title, thread.overview, thread.create_date, comment.id FROM thread  LEFT JOIN comment ON thread.id = comment.thread_id ORDER BY create_date ASC;

ALTER TABLE thread DROP INDEX user_id;
ALTER TABLE comment DROP INDEX thread_id;
ALTER TABLE comment DROP INDEX user_id;
ALTER TABLE favorite DROP INDEX user_id;
ALTER TABLE favorite DROP INDEX thread_id;

ALTER TABLE favorite
DROP COLUMN id;
